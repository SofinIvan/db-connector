package com.infobip.db;

import org.apache.commons.cli.*;

import java.util.HashMap;
import java.util.Map;

import static com.infobip.db.Application.*;

public class ArgumentsParser {

    public Map<String, String> parseParameters(String[] args) {
        Options options = fillOptions();
        CommandLineParser parser = new DefaultParser();
        Map<String, String> params = new HashMap<>();
        CommandLine commandLine;
        try {
            commandLine = parser.parse(options, args);
            params.put(URL, commandLine.getOptionValue(URL));
            params.put(DB, commandLine.getOptionValue(DB));
            params.put(USER, commandLine.getOptionValue(USER));
            params.put(PASSWORD, commandLine.getOptionValue(PASSWORD));
            params.put(QUERY, commandLine.getOptionValue(QUERY));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return params;
    }

    private Options fillOptions() {
        Options options = new Options();

        options.addOption(buildOption(URL, true));
        options.addOption(buildOption(DB, true));
        options.addOption(buildOption(USER, true));
        options.addOption(buildOption(PASSWORD, true));
        options.addOption(buildOption(QUERY, false));

        return options;
    }

    private Option buildOption(String name, boolean required) {
        return Option.builder(name)
                .required(required)
                .hasArg()
                .build();
    }

}
