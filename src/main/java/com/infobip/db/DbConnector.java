package com.infobip.db;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;

public class DbConnector {

    private Connection connection;

    public void executeQuery(String url, String dbName, String user, String password, String query) {
        String connectionUrl = url + ";database=" + dbName + ";user=" + user + ";password=" + password;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            establishConnection(connectionUrl);
            statement = connection.createStatement();
            if (containsIgnoreCase(query, "update") || containsIgnoreCase(query, "delete")) {
                int changed = statement.executeUpdate(query);
                writeToCsvFile(query, changed);
            } else {
                resultSet = statement.executeQuery(query);
                writeToCsvFile(resultSet);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(statement, resultSet);
        }
    }

    public void checkConnection(String url, String dbName, String user, String password) {
        String connectionUrl = url + ";database=" + dbName + ";user=" + user + ";password=" + password;
        Statement stmt = null;
        try {
            establishConnection(connectionUrl);
            String query = "SELECT 1";
            stmt = connection.createStatement();
            stmt.executeQuery(query);
            writeToCsvFile(query, 0);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(stmt, null);
        }
    }

    private void establishConnection(String connectionUrl) throws ClassNotFoundException, SQLException {
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        connection = DriverManager.getConnection(connectionUrl);
    }

    private void writeToCsvFile(String query, int changed) throws IOException {
        File file = new File("SQL_result_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss")) + ".csv");
        FileUtils.writeStringToFile(file, "Query: " + query + "\n" + "Changed: " + changed + "\n", StandardCharsets.UTF_8);
    }

    private void writeToCsvFile(ResultSet resultSet) throws SQLException, IOException {
        File file = new File("SQL_result_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss")) + ".csv");

        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();

        StringBuilder sBuilder = new StringBuilder();
        String comma = ",";

        for (int i = 1; i < columnCount; i++) {
            sBuilder.append(metaData.getColumnName(i)).append(comma);
        }
        sBuilder.append(metaData.getColumnName(columnCount)).append("\n");

        if (file.createNewFile()) {
            while (resultSet.next()) {
                for (int i = 1; i < columnCount; i++) {
                    sBuilder.append(resultSet.getString(i)).append(comma);
                }
                sBuilder.append(resultSet.getString(columnCount)).append("\n");
            }
        }

        FileUtils.writeStringToFile(file, sBuilder.toString(), StandardCharsets.UTF_8);
    }

    private void close(Statement stmt, ResultSet rs) {
        if (rs != null) try {
            rs.close();
        } catch (Exception ignored) {
        }

        if (stmt != null) try {
            stmt.close();
        } catch (Exception ignored) {
        }

        if (connection != null) try {
            connection.close();
        } catch (Exception ignored) {
        }
    }

}
