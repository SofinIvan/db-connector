package com.infobip.db;

import java.util.Map;

import static org.apache.commons.lang3.StringUtils.isEmpty;

public class Application {

    public static final String URL = "url";
    public static final String DB = "db";
    public static final String USER = "user";
    public static final String QUERY = "query";
    public static final String PASSWORD = "password";

    public static void main(String[] args) {
        Map<String, String> parameters = new ArgumentsParser().parseParameters(args);

        String url = parameters.get(URL);
        String db = parameters.get(DB);
        String user = parameters.get(USER);
        String password = parameters.get(PASSWORD);
        String query = parameters.get(QUERY);

        DbConnector connector = new DbConnector();
        if (isEmpty(parameters.get(QUERY))) {
            connector.checkConnection(url, db, user, password);
        } else {
            connector.executeQuery(url, db, user, password, query);
        }
    }

}
